import { Component } from '@angular/core'; 
import { Options } from 'ng5-slider';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormControl } from '@angular/forms';  

export interface Gender {
  name: string;
}

@Component({  
  selector: 'app-root',  
  templateUrl: './app.component.html',  
  styleUrls: ['./app.component.scss']  
})  

export class AppComponent {  
  title="Tunaiku"
  registrationForm: FormGroup;  
  yourName:string='';  
  yourAddress:string=''; 
  birthday:Date=null; 
  gender:string='';
  ktpNumber:number; 
  IsAccepted:number=0;
  inputNik:string='';
  date;
  dateForm : FormGroup;

  period: FormControl = new FormControl();
  options: Options = {
    floor: 6,
    ceil: 20
  }
  loan: FormControl = new FormControl();
  loanOptions: Options = {
    floor: 2,
    ceil: 20
  }

  constructor(private fb: FormBuilder) {  
    this.registrationForm = fb.group({  
      'yourName' : [null, Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z ]*$')])],
      'ktpNumber' : [null, Validators.compose([Validators.required, Validators.min(1000000000000000), Validators.max(9999999999999999)])],
      'yourAddress' : [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(150)])], 
      'birthday' : [null, Validators.required],  
      'gender' : [null], 
      'IsAccepted': [null],
      'period' : [null],
      'loan' : [null]
    }); 

    this.inputNik = 'initial value';
    this.dateForm = new FormGroup({
      date:new FormControl(this.date),
      setDate:new FormControl(),
    })
  }  

  onNikChange(searchValue : string){
    if(searchValue.length == 16){
      let tgl = searchValue.substring(6, 8);
      let bln = searchValue.substring(8, 10);
      let thn = searchValue.substring(10,12);

      if (parseInt(tgl) > 40) {
        this.registrationForm.controls['birthday'].setValue(new Date("19"+thn+"-"+bln+"-"+(parseInt(tgl)-40)));
        this.registrationForm.controls['gender'].setValue("Wanita");
      } else { 
        this.registrationForm.controls['birthday'].setValue(new Date("19"+thn+"-"+bln+"-"+(parseInt(tgl))));
        this.registrationForm.controls['gender'].setValue("Pria");
      }
    }
  }

  getErrorName() {
    return this.registrationForm.controls['yourName'].hasError('required') ? 'Silahkan isi nama Anda' :
      this.registrationForm.controls['yourName'].hasError('pattern') ? 'Isi dengan huruf saja' :
      this.registrationForm.controls['yourName'].hasError('maxlength') ? 'Maksimum 50 kata' :
      '';
  }

  getErrorNik() {
    return this.registrationForm.controls['ktpNumber'].hasError('required') ? 'Silahkan isi nomor ktp Anda' :
      this.registrationForm.controls['ktpNumber'].hasError('min') ? 'Angka kurang dari 16 digit' :
      this.registrationForm.controls['ktpNumber'].hasError('max') ? 'Angka lebih dari 16 digit' :
      '';
  }

  getErrorAddress() {
    return this.registrationForm.controls['yourAddress'].hasError('required') ? 'Silahkan isi alamat Anda' : 
      this.registrationForm.controls['yourAddress'].hasError('minlength') ? 'Alamat kurang dari 3 kata' :
      this.registrationForm.controls['yourAddress'].hasError('maxlength') ? 'Alamat lebih dari 150 kata' :
      '';
  }
    
  onChange(event:any)  
  {  
    if (event.checked == true) {  
      this.IsAccepted = 1;  
    } else {  
      this.IsAccepted = 0;  
    }  
  }  
   
  onFormSubmit(form:NgForm)  
  {  
    form['period'] = (this.period.value);
    form['loan'] = (this.loan.value);
    console.log(form);
  }  
}